module networkBaS
	include("INDS.jl")
	using .INDS, Distributions
	export BallAndStick, Event, BaS

	mutable struct BallAndStick
		connections::Vector{Float64}
		stim::Vector{INDS.Solution}
		anti_chains::INDS.HLT
		somatic_fronts::Vector{Float64}
		# current membrane potential
		v::Float64
		index::Int
	end

	function Event(d::Float64,t::Float64,ii::Int=0)
		return Solution((t-d)/2.,(t+d)/2.,ii)
	end

	const BaS = BallAndStick

	BallAndStick()                      						  = BallAndStick([],[],INDS.HLT(),[],0.,1)
	BallAndStick(d::Distribution{Univariate},N::Int64)            = BallAndStick(rand(d,N),[],INDS.HLT(),[],0.,1)
	BallAndStick(d::Distribution{Univariate},N::Int64,ind::Int64) = BallAndStick(rand(d,N),[],INDS.HLT(),[],0.,ind)
	BallAndStick(d::Function,N::Int64,ind::Int64) = BallAndStick(d.(rand(N)),[],INDS.HLT(),[],0.,ind)

	function update_dendrite(t::Float64,neuron::BaS,ind_neuron::Int64,parms,delay::Float64)
		# this function implements the jump of the process
		# we add fronts in each of the dendrites connected to the neuron that just spiked
		# we increased the spike counter for the neuron that just spiked
		# the spike can be added to the dendrite after a given delay
		if neuron.index == ind_neuron
			neuron.v = 0.
		else
			e = networkBaS.Event(neuron.connections[ind_neuron],t + delay,0) #Event (x_i,t)
			# we update the poset structure for each dendrite
			modified_fronts = INDS.addPoint!(neuron.anti_chains,e,false,true)
			#fronts modifiés, retournés par ordre croissant
			if length(modified_fronts)>0
				for ii in modified_fronts
					if ii>length(neuron.somatic_fronts)
						push!(neuron.somatic_fronts,get( (neuron.anti_chains)[ii].max).Y*2. )
					else
						(neuron.somatic_fronts)[ii] = get( (neuron.anti_chains)[ii].max).Y*2.
					end
				end
			end
			######################################################################
			# this is to remove unnecessary data, to keep memory usage small
			if t  > .2 + parms[1]
				# println("\n--> cleanup at t=$t, t_rm = $(parms[1])")
				# println("----> cleaning #fronts = $(length(neuron.anti_chains)) ~> ")
				ind = searchsortedlast(neuron.somatic_fronts,t-1.02)
				if ind>0
					deleteat!(neuron.somatic_fronts,1:ind)
					deleteat!(neuron.anti_chains,1:ind)
				end
				# println("$(length(neuron.anti_chains))\n")
			end
		end
		return true
	end

	function update_soma!(neuron::BaS,t0,t1,w,Iext)
		# this is for the IF network
		neuron.v += Iext*(t1-t0)
		# il faut ajouter le nombre de fronts
		ind1 = searchsortedfirst(neuron.somatic_fronts,t0)
		ind2 =  searchsortedlast(neuron.somatic_fronts,t1)
		neuron.v += length(ind1:ind2) * w
	end

	# function putV0_d!(neurons,V0)
	# 	for ii = eachindex(neurons)
	# 		neurons[ii].v = V0[ii]
	# 	end
	# end
end #module

# functions used for the soft MF problem with dendrites
@inline function f(x)
    # return x^2
    return  max(x-0.2,0)^8
end

function F(v)
    # champs de vecteur
    lambda_ = 0.1
    return Iext - lambda_*v
end

@inline function flow(t1::Float64,t2::Float64,v::Float64,Iext::Float64 = 1.0)
    # calcul le flot numerique associe au champs de vecteurs F
    # (tout,x,retcode,_) = odecall( radau5, (t,x) -> F(x), t,[v], opt)
    # @assert(tout[end]==t[end],string("temps identiques!",tout," ",t))
    lambda_ = 0.1
    # dsolve({diff(v(t), t) = Iext-lambda*v(t), v(0) = v0})
    return Iext/lambda_ + exp(-lambda_*(t2-t1))*(v - Iext/lambda_) # cas de \dot v = Iext-lambda*v
    return Iext*(t2-t1)+v						 	 # cas de \dot v = Iext
end


function empirical_dist(result,start_time,end_time;interval_v=[0.,3.],npoints_v=150)
    ind = findall((result.time .< end_time) .* (result.time .>= start_time))
    e = vec(LinRange(interval_v[1],interval_v[2],npoints_v+1))
    gg = zeros(length(ind),npoints_v)
    for i in 1:(length(ind))
        gg[i,:] = fit(Histogram,vec(result.xc[:,ind[i]]),e).weights
    end
    return gg,e,result.time[ind]
end

function empirical_dist(time,xc,start_time,end_time;interval_v=[0.,3.],npoints_v=150)
    ind = find((time.<end_time) & (time.>=start_time))
    e = vec(linspace(interval_v[1],interval_v[2],npoints_v+1))
    gg = zeros(length(ind),npoints_v)
    for i in 1:(length(ind))
        gg[i,:] = fit(Histogram,vec(xc[:,ind[i]]),e).weights
    end
    return gg,e,time[ind]
end

function flow_neuron(neuron,t,Iext::Float64, w::Float64)
    # neuron type is supposed to be ::networkBaS.BaS
    # update the membrane potential of the current neuron from time t[1]
    # to time t[2]
    out::Float64 = neuron.v
    t_current = t[1]
    for ind_soma in  searchsortedfirst(neuron.somatic_fronts,t[1]):length(neuron.somatic_fronts)
        t_soma = neuron.somatic_fronts[ind_soma]
        # @assert(t_soma != t_current) # pour se simplifier la vie
        # @assert(t_soma != t[2]) # pour se simplifier la vie
        if t_current < t_soma && t_soma < t[2]
            out = flow(t_current, t_soma,out,Iext)
            # on incremente du fait du front somatique
            out += w
            t_current = t_soma
        end
    end
    # on evolue jusque t[1]
    out = flow(t_current, t[2],out,Iext)
    neuron.v = out
    # println("$out - $(neuron.v)")
    return out
end

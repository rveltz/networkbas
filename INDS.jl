# try
# 	include("/Users/rveltz/work/prog_gd/julia/dev/NonDominatedSorting/TREAP.jl")
# catch
# end
#
# try
# 	include("user/rveltz/home/prog/julia/nds/Treap.jl")
# catch
# end

module INDS
	include("TREAP.jl")
	using Parameters, Nullables
	import Base.show, Base.<, Base.==, Base.print, Base.println
	import Nullables: Nullable
	export Solution, addPoint!, HLT, LLT, LLTNode, println, collect_points_chain

	@with_kw struct Solution
		X::Float64 = 0.
		Y::Float64 = 0.
		ind::Int64 = 0
	end

	<(S1::Solution, S2::Solution) = (S1.X < S2.X)
	==(S1::Solution, S2::Solution) = (S1.X == S2.X) && (S1.Y == S2.Y)
	compareX1(e::Solution, o::Solution) = e.X < o.Y
	compareX2(e::Solution, o::Solution) = e.Y < o.Y
	show(io::IO, t::TREAP.Node{K}) where K = show(io, "Key: $(t.tree_key)")

	const LLT     = TREAP.Treap{Solution}
	const LLTNode = TREAP.Node{Solution}
	const HLT     = Vector{LLT}
	node(s::Solution) = LLTNode(s, s.Y, LLTNode(), LLTNode())

	function print(t::LLTNode)
		if isempty(t)
			return ""
		else
			return "[$(t.tree_key.X) $(t.tree_key.Y)]-"*print(t.left)*print(t.right)
		end
	end

	function print(h::HLT)
		res = ""
		for ii =1:length(h)
			res = res*"\n$(ii-1)\n"*print(h[ii].root)*"\n"
		end
		res
	end

	println(h::HLT)     = println(print(h))
	println(h::LLTNode) = println(print(h))
	println(t::LLT)     = println(print(t.root))

	function splitY(t::LLTNode,k::Solution)
		# This one works!!
		if t.tree_key.Y >= k.Y
			if isempty(t.right)
				r = LLTNode()
				newTree = LLTNode()
			else
				newTree,r = splitY(t.right,k)
			end
			l = LLTNode(t.tree_key,t.heap_key,t.left,newTree)
		else
			if isempty(t.left)
				l = LLTNode()
				newTree = LLTNode()
			else
				l,newTree = splitY(t.left,k)
			end
			r = LLTNode(t.tree_key,t.heap_key,newTree,t.right)
		end
		return l,r
	end

	splitY(t::LLTNode,k::LLTNode) = splitY(t,k.tree_key)

	splitX(t::LLTNode,k::Solution) = split(t,k)
	splitX(t::LLTNode,k::LLTNode)  = split(t,k.tree_key)
	splitX(t::LLT,k::Solution)     = split(t.root,k)
	splitX(t::LLT,k::LLTNode)      = split(t.root,k.tree_key)


	function compareDom(s1::Solution,s2::Solution)
		xc = sign(s1.X-s2.X)
		yc = sign(s1.Y-s2.Y)
		return xc + yc
	end

	function getMinP(t::LLTNode)
		return Solution(TREAP.minimum(t).X,TREAP.maximum(t).Y,0)
	end
	getMinP(t::LLT) = getMinP(t.root)

	function dominatedBySomebody(t::LLT,s::Solution)
		l,r = splitX(t,s)

		# l1,r1 = LLTNode(), LLTNode()
		# split!(t.root,s,l1,r1)
		# @assert (l1==l) && (r1==r)
		if (!isempty(l) && compareDom(s,TREAP.maximum(l)) > 0.)
			return true
		end
		if (!isempty(r) && compareDom(s,TREAP.minimum(r)) > 0.)
			return true
		end
		return false
	end

	function determineRankStupid(h::HLT,s::Solution)
		currRank = 0
		for i = 1:length(h)
			if dominatedBySomebody(h[i],s)
				currRank = i
			else
				return currRank
			end
		end
		return currRank
	end

	function determineRank(h::HLT,s::Solution,BaS=false)
		# BaS = true, simplifies rank determination by saying s.Y - get(h[1].max).Y > 1
		currRank = 0
		l = 1
		while BaS && length(h)>0 && l<=length(h) && get(h[l].max).Y < s.Y-1.5
			l += 1
		end
		r = length(h)# etait -1
		i::Int64 = 0
		while (l <= r)
			# println(l+r," ",(l+r) >> 2)
			i = (l + r) >> 1
			# if dominatedBySomebody(h[i],s)
			if LowLevelDominates(h[i].root,s)
				currRank = i
				l = i + 1
			else
				r = i - 1
			end
		end
		# @assert determineRankStupid(h,s) == currRank "Error current = $currRank, vs $(determineRankStupid(h,s))"
		return currRank
	end

	function LowLevelDominates(t::LLTNode,s::Solution)
		# returns whether any solution from T dominates s
		# T : LLTNODE the root node of the low-level tree
		# s : SOLUTION
		# the solution to test for domination
		b = LLTNode() # the best node so far
		while isempty(t) == false
			if t.tree_key.X <= s.X
				b = t
				t = t.right
			else
				t = t.left
			end
		end
		if isempty(b)
			return false
		else
			return (b.tree_key.Y < s.Y) || (b.tree_key.Y == s.Y) && (b.tree_key.X < s.X)
		end
	end

	function collect_points_chain!(res,t::LLTNode)
		if isempty(t)
			return
		end
		push!(res,[t.tree_key.X,t.tree_key.Y])
		collect_points_chain!(res,t.right)
		collect_points_chain!(res,t.left)
		return
	end

	function addPoint!(h::HLT,s::Solution,verbose=false, BaS = false)
		# verbose = true: allows to print debugging messages
		# BaS = true, simplifies rank determination by saying s.Y - get(h[1].max).Y > 1
		# return the index of the modified front in index_return
		rank = determineRank(h,s,BaS)
		nTreap = LLTNode(s)
		i::Int64 = 0

		if (rank >= length(h))
			push!(h,LLT(nTreap.tree_key))
			return [length(h)]
		elseif compareDom(s,getMinP(h[rank+1])) < 0.
			insert!(h,rank+1,LLT(nTreap.tree_key))
			return [rank+1]
		else
			i = 0
			minP = LLTNode(s)
			cNext = nTreap
			index_return = Int64[]
			while !isempty(minP)
				if (length(h) <= rank + i)
					@assert typeof(nTreap) == LLTNode
					tmpTreap = LLT()
					tmpTreap.root = cNext
					tmpTreap.max = Nullable(cNext.tree_key)
					push!(h,tmpTreap)
					break
				end
				t1l,t1r = splitX(h[rank + i + 1].root,minP)
				trl,trr = LLTNode(), LLTNode() #could be changed by avoiding reallocating
				if !isempty(t1r)
					trl, trr = splitY(t1r,minP)
				end

				res = TREAP.merge(t1l, cNext)
				res = TREAP.merge(res, trr)
				h[rank + i + 1].root = res
				h[rank + i + 1].max = Nullable(TREAP.maximum(res))
				cNext = trl
				push!(index_return,rank + i + 1)
				if isempty(cNext)
					break
				end
				minP = LLTNode(getMinP(cNext))
				i += 1
			end
			return index_return
		end
	end

	function addPoint!(h::HLT,s::Vector{Solution},verbose=false)
		for s1 in s
			addPoint!(h,s1,verbose)
		end
	end
end # end module


#########################################################################
# Affichage processus de Hammersley
function draw_kink(e1,e2)
	Plots.plot!([e1[1], e2[1]],[e1[2], e1[2]],color=:black,leg=false)
	Plots.plot!([e2[1], e2[1]],[e1[2], e2[2]],color=:black,leg=false)
end

function plot_chain(t, c = :green)
	pt = Vector{Float64}[]
	INDS.collect_points_chain!(pt,t)
	pt = pt[sortperm(map(i->i[1],pt))]
	# Plots.plot!([pt[1][1], pt[1][1]],[pt[1][2],1.0],leg=false,color=:black,leg=false)
	Plots.scatter!([pt[1][1]],[pt[1][2]],color=:red)
	for ii=1:length(pt)-1
		draw_kink(pt[ii],pt[ii+1])
		Plots.scatter!([pt[ii][1]],[pt[ii][2]],color=:red)
	end
	Plots.scatter!([pt[end][1]],[pt[end][2]],color=:red)
	# Plots.plot!([pt[end][1], 1.],[pt[end][2],pt[end][2]],leg=false,color=:black,leg=false)
end

function plot_chains(h,color=:red)
	for ii in 1:length(h)
		plot_chain(h[ii].root,color)
	end
end

########################################################################
## Case rotated
function draw_kink_rotated(e1,e2,subp=1)
	dt = (e2[2] - e1[2] + e1[1] + e2[1])/2.
	Plots.plot!([e1[1], dt],[e1[2], e1[2] + dt - e1[1]],color=:black,leg=false,subplot=subp)
	Plots.plot!([e2[1], dt],[e2[2], e2[2] - dt + e2[1]],color=:black,leg=false,subplot=subp)
end

function plot_chain_rotated(t, c = :green,upper_bound=1.,subp=1,m=2)
	pt = Vector{Float64}[]
	INDS.collect_points_chain!(pt,t)
	pt = pt[sortperm(map(i->i[2],pt))]
	pt2 = map(pt->[pt[1]+pt[2],-pt[1]+pt[2]], pt)
	# pt2 = pt2[sortperm(map(i->i[2],pt2),rev=true)]
	plot!([pt2[end][1],upper_bound-pt2[end][2]+pt2[end][1]],[pt2[end][2],upper_bound],color=:red,subplot=subp)
	for ii=1:length(pt)-1
		draw_kink_rotated(pt2[ii],pt2[ii+1],subp)
	end
	plot!([pt2[1][1],0 + pt2[1][2] + pt2[1][1] ],[pt2[1][2],0.],color=:green,subplot=subp)
	Plots.scatter!(map(v->v[1]+v[2],pt),map(v->-v[1]+v[2],pt),color=c,leg=false,subplot=subp,m=m,markershape=:none,markerstrokealpha = 0)
end


function plot_chains_rotated(h,c=:red;upper_bound=1.0,subplot=1,m=2)
	for ii in 1:length(h)
		plot_chain_rotated(h[ii].root,c,upper_bound,subplot,m)
	end
end

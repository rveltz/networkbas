# Information

This is a simplified version of the code needed to reproduce the simulations in the article N. Fournier, E. Tanré, and R. Veltz **On a toy network of neurons interacting through their dendrites Annales de l’Institut Henri Poincaré (B)** Probabilités et Statistiques, 2019.

It is simplified because it runs in serial whereas for the paper, we ran it on a cluster of 200 processes.

## Installation
To use the code, please install [PiecewiseDeterministicMarkovProcesses.jl](https://github.com/rveltz/PiecewiseDeterministicMarkovProcesses.jl) and run in the current folder.

## Main program

The code is included in the file `network-nds.jl` which should be ran with care. Do not increase too much the network size `N` above `10000`.
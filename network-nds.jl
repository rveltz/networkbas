using Revise

include("INDS.jl")

using Plots, Distributions, Parameters, ProgressMeter, StatsBase, SparseArrays
using PiecewiseDeterministicMarkovProcesses
const PDMP = PiecewiseDeterministicMarkovProcesses
using .INDS

include("networkBaS.jl")
include("soft.jl")
# include("rejection_exact.jl")

const N = 2_000 # number of neurons;
# rnd_connections = Distributions.Truncated(Distributions.Cauchy(1.,1.),0.,1.)
# rnd_connections = Distributions.Truncated(Uniform(),0,1)
rnd_connections = v->1-sqrt(1-v) # for p(x)dx = 2(1-x)
println("--> initialisation du reseau...")
# srand(123)
const network = [networkBaS.BaS(rnd_connections,N,i) for i=1:N];
const p_bar = Progress(6000)
println("---->OK!")



# const connections = rand(N,N)*0.2 # hauteur des connections dans les dendrites
# connections = rand(Distributions.Truncated(Normal(0., 0.3), 0., 1.),N,N)

# @assert(1==0,"Attention! D = 1 car sinon poset_dendrite bug")
const w       = 1.0/sqrt(N) # dépolarisation causée par l'arrivée d'un front sur le soma
const Iext    = 1.0

function Phi(out::Array{Float64,2}, xc::Vector{Float64},xd::Array{Int64},t::Array{Float64},parms::Vector{Float64})
    # vector field used for the continuous variable
    # le flow est connu entre chaque front somatique
    out[1,:] .= xc
    for i = 1:N
        # fait evoluer le network[i].v accordingly
        out[2,i] = flow_neuron(network[i],t,Iext,w)
    end
    nothing
end

function R_mf_rejet(rate::Vector{Float64},xc::Vector{Float64},xd::Array{Int64},t::Float64,parms::Vector{Float64}, sum_rate::Bool)
    bound::Float64 = N * 1#1.5 works well [1.48 1.4 1.38] for N=1000
    # if t>0.1
        bound = N * 5.2
    # end
    # if t>1.7
        # bound = N * 3.5
    # end

    print_rate::Float64 = 0.
    # rate function
    if sum_rate == false
        for i=1:N
            rate[i] = f(xc[i])
        end
        rate[N+1] = print_rate
        return 0., bound
    else
        res = print_rate
        for i=1:N
            res += f(xc[i])
        end
        return res, bound
    end
end

function Delta_xc_mf(xc::Array{Float64,1},xd::Array{Int64},t::Float64,parms::Vector{Float64},ind_reaction::Int64)
    # this function makes the jump of the process
    # we add fronts in each of the dendrites connected to the neuron that just spiked
    # we increased the spike counter for the neuron that just spiked
    if ind_reaction <= N
        # println("--> jump at t = ",t," ",ind_reaction)
        # Threads.@threads for i=1:N
        for i=1:N
            networkBaS.update_dendrite(t,network[i],ind_reaction,parms,0.)
            xc[i] = network[i].v
        end
        xd[ind_reaction] += 1
    end
    # this is to remove unnecessary data, to keep memory usage small
    if t  > .2 + parms[1]
        parms[1] += .2 #saving floor(t) for update purposes
    end

    # just to know how much computation is left
    next!(p_bar; showvalues = [(:t,round(t,digits = 4) )])
    return true
end

xc0 = rand(N)*1.
xd0 = Vector{Int64}(zeros(N+1))

const nu_tcp = spzeros(Int64,N+1,N+1) # we don't need it here
parms = [0., 0.] #used to store [E[t],nbstep]
const tf = 20000.7

result = @time rejection_exact(5,xc0,xd0,Phi,R_mf_rejet,Delta_xc_mf,
        nu_tcp,parms,0.0,tf,false,false)
println("Done!!\n")

@assert 1==0

p_bar.n = 1000
    # srand(123)
    p_bar.counter = 0
    println("--> Calcul... N = $N, #steps = $(p_bar.n)")
    for i=1:N
        empty!(network[i].stim)
        empty!(network[i].somatic_fronts)
        network[i].anti_chains = INDS.HLT()
        network[i].v = xc0[i]
    end
    parms = [0., 0.]
    result = @time PDMP.rejection_exact(p_bar.n,xc0,xd0,Phi,R_mf_rejet,
    Delta_xc_mf,nu_tcp,parms,0.0,tf,false,false)

tt = result.time
rej_b = zeros(length(tt),1)
@time @showprogress for i in 1:(length(tt))
	xc_v = @view result.xc[:,i]
	rej_b[i] = sum(f.(xc_v))/N
end

plotlyjs()

if 1==1
    tt = result.time
    rej_b = zeros(length(tt),1)
    @time @showprogress for i in 1:(length(tt))
        xc_v = @view result.xc[:,i]
        rej_b[i] = sum(f.(xc_v))/N
    end
    Plots.plot(tt,rej_b,legend=false,title= "rejet - g(v,t) for N = $N, lambda = , Njmp = $(length(tt))")
end

ylims!(0,3.5)

if 1==1
    #plot empirical distribution
    gg,e,tt = empirical_dist(result,0.0,result.time[end],npoints_v=100)
    Plots.heatmap(tt,e[2:end],gg',title=string("rejet - g(v,t) for N = ",N, ", lambda = "),color=:bluesreds,xlim=[0.,tt[end]],ylim=[0.,2.],fill=true)
end

# http://coldattic.info/shvedsky/pro/blogs/a-foo-walks-into-a-bar/posts/4
# https://tanujkhattar.wordpress.com/2016/01/10/treaps-one-tree-to-rule-em-all-part-1/
# http://kukuruku.co/hub/cpp/randomized-binary-search-trees
# https://github.com/ChrisRackauckas/LinkedLists.jl/blob/master/src/list.jl
module TREAP
	using Nullables

    import Base.isempty, Base.copy, Base.split, Base.insert!, Base.<, Base.==, Base.size, Base.copy!#, Base.max

    const PriorityT = Float64

    export NodeImpl, Node, Treap, split, split!, exists, merge, insert!, height, size, minimum, maximum, root, copy#, max

    mutable struct NodeImpl{K}
        heap_key::PriorityT
        tree_key::K
        left::NodeImpl{K}
        right::NodeImpl{K}
        NodeImpl{K}()       where K = new(PriorityT(-Inf))
		NodeImpl{K}(key)    where K = new(rand(PriorityT),key,NodeImpl{K}(),NodeImpl{K}())
        NodeImpl{K}(key, priority, left, right) where K = new(priority, key, left, right)
    end

    NodeImpl(key::K) where K = NodeImpl{K}(key)#new(rand(PriorityT),key,NodeImpl{K}(),NodeImpl{K}())

    const Node{K}     = NodeImpl{K}
    isempty(t::Node)  = t.heap_key == PriorityT(-Inf)
    copy(t::Node{K}) where K= Node{K}(t.tree_key::K,t.heap_key,t.left,t.right)

    mutable struct Treap{K}
        root::Node{K}
        max::Nullable{K} #maximum of the treap
        Treap{K}()       where K = new(Node{K}(),   Nullable{K}())
        Treap{K}(key::K) where K = new(Node{K}(key),Nullable(key))
    end


    function ==(L::Node{K}, R::Node{K}) where K
        if isempty(L) && isempty(R)
            return true
        elseif isempty(L) || isempty(R)
            return false
        else
            return (L.tree_key == R.tree_key) && (L.left == R.left) && (L.right == R.right)
        end
    end

    function exists(treap::Node{K},k::K) where K
        if isempty(treap)
            return false
        end
        if k == treap.tree_key
            return true
        end
        return exists(k < treap.tree_key ? treap.left : treap.right, k)
    end

    function copy!(treap::Node{K},new_node::Node{K}) where K
        treap.heap_key = new_node.heap_key
        treap.tree_key = new_node.tree_key
        treap.left     = new_node.left
        treap.right    = new_node.right
    end

    function merge(L::Node{K},R::Node{K}) where K
        # println("--> TreaP.merge")
        isempty(L)  && return R
        isempty(R)  && return L
        if L.heap_key > R.heap_key
            newR = merge(L.right,R)
            return Node{K}(L.tree_key,L.heap_key,L.left,newR)
        else
            newL = merge(L,R.left)
            return Node{K}(R.tree_key,R.heap_key,newL,R.right )
        end
    end

    function split(t::Node{K},k::K) where K
        # println("--> TreaP.split")
        if t.tree_key < k
            if isempty(t.right)
                r = Node{K}()
                newTree = Node{K}()
            else
                newTree,r = split(t.right,k)
            end
            l = Node{K}(t.tree_key,t.heap_key,t.left,newTree)
        else
            if isempty(t.left)
                l = Node{K}()
                newTree = Node{K}()
            else
                l,newTree = split(t.left,k)
            end
            r = Node{K}(t.tree_key,t.heap_key,newTree,t.right)
        end
        return l,r
    end

    function split(t::Node{K},k::Node{K}) where K
        return split(t,k.tree_key)
    end

    function split!(t::Node{K},key::K,l::Node{K},r::Node{K}) where K
        println("--> TreaP.split!")
        # Warning this function modifies t!!!
        if isempty(t)
            l.heap_key = PriorityT(-Inf)
            r.heap_key = PriorityT(-Inf)
        elseif t.tree_key < key
            copy!(l,t)
            split!(t.right,key,t.right,r)
        else
            copy!(r,t)
            split!(t.left,key,l,t.left)
            # copy!(r,t)
        end
    end

    function split!(t::Node{K},k::Node{K},l::Node{K},r::Node{K}) where K
        return split!(t,k.tree_key,l,r)
    end

    # # add a key to the treap
    # function add!(t::Node{K},key::K) where K
    #   println("t=$t")
    #   if isempty(t)
    # 		t.tree_key = key
    # 		t.heap_key = rand(PriorityT)
    # 		t.left = Node{K}()
    # 		t.right = Node{K}()
    # 		return t
    # 	end
    #   l = Node{K}()
    #   r = Node{K}()
    #   l,r = split(t,key)
    #   println("--> ",l,"\n",r)
    #   m = Node{K}(key)
    #   println("m=",merge(merge(l,m),r))
    #   copy!(t,merge(merge(l,m),r))
    # end

    function insert!(t::Node{K},n::Node{K}) where K
        # println("--> TreaP.insert!")
        if isempty(t)
            copy!(t,n)
        elseif n.heap_key <= t.heap_key
            n.left,n.right = split(t,n.tree_key)
            copy!(t,n)
        else
            insert!(n.tree_key < t.tree_key ? t.left : t.right,n)
        end
    end

    function height(treap::Node)
        if isempty(treap)
            return 0
        end
        lh = height(treap.left)
        rh = height(treap.right)
        return 1+((lh<rh) ? rh : lh)
    end

    function size(treap::Node)
        # return Node.
        if isempty(treap)
            return 0
        end
        return 1 + size(treap.left) + size(treap.right)
    end

    function minimum(t::Node)
        isempty(t) && error("An empty treap has no minimum.")
        while !isempty(t.left) t = t.left end
        t.tree_key
    end

    function maximum(t::Node)
        isempty(t) && error("An empty treap has no maximum.")
        while !isempty(t.right) t = t.right end
        t.tree_key
    end

    function insert!(t::Treap{K},k::Node{K}) where K
        insert!(t.root,k)
        if isnull(t.max)
            t.max = Nullable(k.tree_key)
            return #for type stability
        elseif k.tree_key > get(t.max)
            t.max = Nullable{K}(k.tree_key,true)
            return
        end
        return #for type stability
    end

    isempty(t::Treap)          = isempty(t.root)
    size(t::Treap)             = size(t.root)
    height(t::Treap)           = height(t.root)
    minimum(t::Treap)          = minimum(t.root)
    maximum(t::Treap)          = maximum(t.root)
    split(t::Treap{K},k::K) where K = split(t.root,k)
    merge(t1::Treap,t2::Treap) = merge(t1.root,t2.root)
    root(t::Treap) = t.root
    exists(key::K, t::Treap{K}) where K = exists(key, t.root)
    max(t::Treap{K}) where K = get(t.max)
end

function plot_t(t,x,y,r = 5.)
    if isempty(t)
        return
    end
    Plots.plot!([x, x-r],[y, y-r],color=:black)
    Plots.plot!([x ,x+r],[y, y-r],color=:black)
    Plots.scatter!([x],[y],marker=(10,0.99,:white),leg=false)
    Plots.annotate!(x,y,Plots.text("$(t.tree_key)\n$(round(t.heap_key,digits=3))",5))
    plot_t(t.left,x-r,y-r,r/1.25)
    plot_t(t.right,x+r,y-r,r/1.25)
end
